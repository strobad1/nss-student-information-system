O projektu: https://docs.google.com/document/d/1u3tvG7hGSAdV7j9fcvn6ugqCJ0ObUvoGiGRgc_X-SvU/edit

## Návod pro spuštění
#### 1) Naklonovat tento repozitář
```
git@gitlab.fel.cvut.cz:strobad1/nss-student-information-system.git
```
#### 2) Spustit všechny docker containery
Přejděte do adresáře obsahujícího soubor **docker-compose.yml** a spusťte všechny služby.

Popřípadě pomocí příkazu:
```
docker-compose up -d
```

Následně se spustí všechny tyto služby:
- Zookeeper na portu 2181
- Kafka Broker na portu 9092
- Prometheus na portu 9090
- Grafana na portu 3000

#### 3) Spustit Elasticsearch (Ujištěte se, že máte více než 5% volného místa na disku na kterém máte tento repozitář, jinak se objeví chyba)
Přejděte do adresáře **/elasticSearchFiles/elasticsearch-7.17.21/bin**, a spusťte soubor **elasticsearch.bat** na Windows nebo jeho stejně pojmenovanný ekvivalent pro Linux.

#### 4) Spustit všechny services v tomto pořadí a dejte každé dostatečně času na spuštění
1. DiscoveryApplication
2. NotificationServiceApplication (měl by spolupracovat s dockerem)
3. EnrollmentApplication
4. SisApplication

poznámka: pokud se při 4. kroku objeví následující problém:
503 SERVICE_UNAVAILABLE from POST http://enrollment-service:8081/enrollment/bielevan [DefaultWebClient]

pak se nejspíše vygeneroval špatně enrollmentDB.lock.db, nastavte v tom souboru hostName na "localhost" a mělo by být OK

#### 5) Spustit frontend aplikaci
Přejděte do složky **REACT frontend** a nainstalujte potřebné závislosti pomocí:
```
npm install
```
Spusťte pomocí příkazu:
```
npm start
```

#### 6) Inicializace
Po spuštění všech služeb a Elasticsearch se při spuštění programu automaticky inicializuje databáze následujícími položkami:

- Admin účet s přihlašovacími údaji:
    - Uživatelské jméno: admin
    - Heslo: 123
- Učitelé:
    - Uživatelské jméno: **sebekji1**
    - Heslo: **123**
- Studenti:
    - Uživatelská jména: strobad1, bielevan, leductha, hoanglon
    - Heslo pro každého uživatele: 123
- Předměty:
    - Návrh Softwarových Systémů (B6B36NSS)
    - Programování v Jazyce Java (B6B36PJV)

#### 7) Použití aplikace
1. Přihlašte se za jednoho ze studentů pomocí uživatelského jména a hesla.
2. Klikněte na View Enrollment Report
3. Příhlašte se za učitele
4. Ohodnoťte jednoho ze studentů - na jeho email přijde upozornění
5. Přihlašte se za studenta a opět se podívejte na Report

Nebo

Využitím Postmanu a requestů ve složce /postmanRequests proklikejte endpointy v pořadí ve kterém jsou.


## Semestrální práce body k odevzdání na konci semestru

| Požadavek | Stav | Popis použití                                                                      |
|----------|------|------------------------------------------------------------------------------------|
| Výběr vhodné technologie a jazyka | ✅ | Použita technologie Java/SpringBoot                                                |
| Readme v gitu s popisem co je hotové | ✅ |                                                                                    |
| Využití společné DB | ✅ | Použita H2 databáze                                                                |
| Využití cache | ✅ | Při fetchování uživatelových známek                                                |
| Využití messaging principu | ✅ | Použita Kafka při odesílání emailu při oznámkování                                 |
| Zabezpečení | ✅ | Basic authorization                                                                |
| Využití Interceptorů | ✅ | Logování requestů                                                                  |
| Využití REST | ✅ | API postaveno na REST architektuře                                                 |
| Nasazení na produkční server například Heroku | ❌ | - neměli jsme na cvičení.                                                          |
| Výběr vhodné architektury | ✅ | Použita architektura event-driven                                                  |
| Inicializační postup | ✅ | Popsáno v dokumentaci                                                              |
| Využití Elasticsearch | ✅ | Implementováno pro full-textové vyhledávání                                        |
| Použití alespoň 5 design patternů | ✅ | Implementováno v Enrollment Loggeru (adapter decorator factory singleton strategy) |
| Za každého člena týmu 2 UC | ✅ | Popsáno v dokumentaci                                                              |



